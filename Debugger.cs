﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;

public static class Debugger
{
    public static bool useLog = true;

    public static string projectFolder = string.Empty;
    public static LogLevel level = LogLevel.All;
    public static readonly List<IDebugger> debuggers = new List<IDebugger>();

    static Debugger() {
        AppDomain.CurrentDomain.UnhandledException += (sender, e) => {
            if (e == null || e.ExceptionObject == null) {
                return;
            }

            try
            {
                if (e.ExceptionObject.GetType() != typeof(System.Exception))
                {
                    return;
                }
            }
            catch
            {
                Log(string.Empty, "Debugger: Failed to report uncaught exception");
                return;
            }

            LogException(string.Empty, (Exception)e.ExceptionObject);
        };
    }

    public static void Message(string name, LogLevel level, string msg, string trace)
    {
        if (!useLog) return;
        if ((Debugger.level & level) == 0) return;
        LogEventArgs args = new LogEventArgs(name, level, msg, trace);
        for (int i = 0; i < debuggers.Count; i++)
        {
            IDebugger debugger = debuggers[i];
            if (!debugger.useLog || (debugger.level & level) == 0) continue;
            debugger.Message(args);
        }
    }

    public static void MessageException(string name, Exception e) {
        if (!useLog || (Debugger.level & LogLevel.Exception) == 0) return;
        LogEventArgs args = new LogEventArgs(name, e);
        for (int i = 0; i < debuggers.Count; i++)
        {
            IDebugger debugger = debuggers[i];
            if (!debugger.useLog || (debugger.level & LogLevel.Exception) == 0) continue;
            debugger.Message(args);
        }
    }

    public static void LogTrace(string name, object obj) {
        LogTrace(name, obj == null ? "Null" : obj.ToString());
    }

    public static void LogTrace(string name, string str, params object[] param)
    {
        Message(name, LogLevel.Trace, param.Length > 0 ? string.Format(str, param) : str, GetStackTrace());
    }

    public static void Log(string name, object obj) {
        Log(name, obj == null ? "Null" : obj.ToString());
    }

    public static void Log(string name, string str, params object[] param)
    {
        Message(name, LogLevel.Log, param.Length > 0 ? string.Format(str, param) : str, GetStackTrace());
    }

    public static void LogInfo(string name, object obj) {
        LogInfo(name, obj == null ? "Null" : obj.ToString());
    }

    public static void LogInfo(string name, string str, params object[] param)
    {
        Message(name, LogLevel.Info, param.Length > 0 ? string.Format(str, param) : str, GetStackTrace());
    }

    public static void LogWarning(string name, object obj) {
        LogWarning(name, obj == null ? "Null" : obj.ToString());
    }

    public static void LogWarning(string name, string str, params object[] param)
    {
        Message(name, LogLevel.Warning, param.Length > 0 ? string.Format(str, param) : str, GetStackTrace());
    }

    public static void LogError(string name, object obj) {
        LogError(name, obj == null ? "Null" : obj.ToString());
    }

    public static void LogError(string name, string str, params object[] param)
    {
        Message(name, LogLevel.Error, param.Length > 0 ? string.Format(str, param) : str, GetStackTrace());
    }

    public static void Assert(string name, bool value, object obj) {
        Assert(name, value, obj == null ? "Null" : obj.ToString());
    }

    public static void Assert(string name, bool value, string str, params object[] param)
    {
        if (value) return;
        Message(name, LogLevel.Assert, param.Length > 0 ? string.Format(str, param) : str, GetStackTrace());
    }

    public static void LogException(string name, Exception e)
    {
        if (e == null) return;
        MessageException(name, e);
    }

    public static void LogException(string name, string str, string trace) {
        Message(name, LogLevel.Exception, str, trace);
    }

    public static string GetStackTrace()
    {
        StringBuilder sb = new StringBuilder();

        StackTrace trace = new StackTrace(true);
        for (int i = 0; i < trace.FrameCount; i++)
        {
            StackFrame frame = trace.GetFrame(i);
            MethodBase method = frame.GetMethod();
            if (method == null || method.DeclaringType == null)
            {
                continue;
            }

            Type declaringType = method.DeclaringType;
            string str = declaringType.Namespace;

            if (declaringType == typeof(Debugger))
            {
                continue;
            }

            if (!string.IsNullOrEmpty(str))
            {
                sb.Append(str);
                sb.Append(".");
            }

            sb.Append(declaringType.Name);
            sb.Append(":");
            sb.Append(method.Name);
            sb.Append("(");
            ParameterInfo[] parameters = method.GetParameters();
            bool flag = false;
            for (int index = 0; index < parameters.Length; index++)
            {
                if (flag)
                {
                    sb.Append(", ");
                }
                else
                {
                    flag = true;
                }

                sb.Append(parameters[index].ParameterType.Name);
            }

            sb.Append(")");

            string fileName = frame.GetFileName();
            if (fileName != null)
            {
                fileName = fileName.Replace('\\', '/');
                sb.Append(" (at ");

                if (fileName.StartsWith(projectFolder))
                {
                    fileName = fileName.Substring(projectFolder.Length, fileName.Length - projectFolder.Length);
                }

                sb.Append(fileName);
                sb.Append(":");
                sb.Append(frame.GetFileLineNumber().ToString());
                sb.Append(")");
            }

            if (i != trace.FrameCount - 1)
            {
                sb.Append("\n");
            }
        }

        return sb.ToString();
    }
}