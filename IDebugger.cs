﻿public interface IDebugger
{
    LogLevel level { get; set; }

    bool useLog { get; set; }

    void Message(LogEventArgs args);
}