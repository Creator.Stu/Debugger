﻿using System;

[Flags]
public enum LogLevel
{
    Off = 0,
    Trace = 1,
    Log = 2,
    Info = 4,
    Warning = 8,
    Error = 16,
    Assert = 32,
    Exception = 64,
    AllOfLog = Trace | Log | Info,
    AllOfError = Error | Assert | Exception,
    All = Trace | Log | Info | Warning | Error | Assert | Exception,
}

