﻿using System;

public class LogEventArgs : EventArgs
{
    public string name { get; private set; }
    public LogLevel Level { get; private set; }
    public DateTime Time { get; private set; }
    public string Message { get; private set; }
    public string StackTrace { get; private set; }
    public Exception Exception { get; private set; }

    public LogEventArgs(string name, LogLevel level, string msg, string trace)
    {
        this.name = name;
        this.Time = DateTime.Now;
        this.Level = level;
        this.Message = msg;
        this.StackTrace = trace;
        this.Exception = null;
    }

    public LogEventArgs(string name, Exception e) {
        this.name = name;
        this.Time = DateTime.Now;
        this.Level = LogLevel.Exception;
        this.Message = e.Message;
        this.StackTrace = e.StackTrace;
        this.Exception = e;
    }
}